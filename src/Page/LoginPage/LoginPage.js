import React from "react";
import { Button, Form, Input /*message*/ } from "antd";
//import { userService } from "./../../services/userServices";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
// import { SET_USER_INFOR } from "./../../redux/constant/userConstant";
//  import { userLocalService } from "../../services/localStorageService";
// Import Lottie
import Lottie from "lottie-react";
import bg_animate from "../../assets/120770-kangaroos-pulling-sleigh.json";
import {
  // setLoginAction,
  setLoginActionService,
} from "./../../redux/actions/userAction";

export default function LoginPage() {
  let navigate = useNavigate();
  let dispatch = useDispatch();
  // const onFinish = (values) => {
  //   console.log("Navigate:", values);
  //   userService
  //     .postDangNhap(values)
  //     .then((res) => {
  //       dispatch(setLoginAction(res.data.content));
  //       console.log(res);
  //       message.success("Đăng nhập được rùiiiii");

  //       // Lưu vào Local Storage
  //       userLocalService.set(res.data.content);

  //       // Delay 1s để show thông báo ~~ 1000 chính là 1s --  window.location.href = "/" sẽ làm trang web phải reload lại toàn bộ
  //       setTimeout(() => {
  //         // window.location.href = "/";
  //         navigate("/");
  //       }, 1000);
  //     })
  //     .catch((err) => {
  //       console.log(err);
  //       message.error("Lỗi rồiiii");
  //     });
  // };

  const onFinishReduxThunk = (value) => {
    let onNavigate = () => {
      setTimeout(() => {
        navigate("/");
      }, 1000);
    };
    dispatch(setLoginActionService(value, onNavigate));
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <div className="flex justify-center items-center h-screen w-screen">
      <div className="container p-5 flex">
        <div className="h-full w-1/2">
          <Lottie animationData={bg_animate} loop={true} />
        </div>
        <div className="h-full w-1/2 pt-24">
          {/* Form */}
          <Form
            name="basic"
            labelCol={{ span: 8 }}
            wrapperCol={{ span: 24 }}
            initialValues={{ remember: true }}
            onFinish={onFinishReduxThunk}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
            layout="vertical"
          >
            <Form.Item
              label="Username"
              name="taiKhoan"
              rules={[
                { required: true, message: "Please input your username!" },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="Password"
              name="matKhau"
              rules={[
                { required: true, message: "Please input your password!" },
              ]}
            >
              <Input.Password />
            </Form.Item>

            <Form.Item className="text-center" wrapperCol={{ span: 16 }}>
              <Button
                type="primary"
                htmlType="submit"
                className="bg-orange-500 text-white hover:bg-white"
              >
                Submit
              </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    </div>
  );
}
