import React from "react";
import { Card } from "antd";
import { NavLink } from "react-router-dom";
// const { Meta } = Card;

export default function MovieList({ movieArr }) {
  const renderMovieList = () => {
    return movieArr.slice(0, 15).map((item) => {
      return (
        <div>
          <Card
            hoverable
            style={{
              width: "100%",
            }}
            cover={
              <img
                className="h-80 object-cover"
                alt="example"
                src={item.hinhAnh}
              />
            }
          >
            {/* <Meta
              title={item.tenPhim}
              description={
                item.moTa.length < 60
                  ? item.moTa
                  : item.moTa.slice(0, 60) + "..."
              }
            /> */}

            <NavLink to={`/detail/${item.maPhim}`}className="bg-red-500 px-5 py-2 rounded text-white shadow hover:shadow-xl transition hover:text-indigo-400">
              Xem chi tiết
            </NavLink>
          </Card>
        </div>
      );
    });
  };
  return <div className="grid grid-cols-5 gap-5">{renderMovieList()}</div>;
}
