import React from "react";
// import "./notFound.css";

export default function NotFoundPage() {
  return (
    <div className="flex justify-center items-center h-screen w-screen">
        {/* Phần code dưới dùng khi có cả file notFound.css */}
      {/* <div className="container">
        <div className="error404page">
          <div className="newcharacter404">
            <div className="chair404" />
            <div className="leftshoe404" />
            <div className="rightshoe404" />
            <div className="legs404" />
            <div className="torso404">
              <div className="body404" />
              <div className="leftarm404" />
              <div className="rightarm404" />
              <div className="head404">
                <div className="eyes404" />
              </div>
            </div>
            <div className="laptop404" />
          </div>
        </div>
      </div> */}
      <h2 className="text-2xl text-red-600 font-bold animate-ping">404</h2>
    </div>
  );
}
