import React from "react";
import Footer from "../Components/Footer/Footer";
import Header from "../Components/Header/Header";

export default function Layout({ children }) {
  return (
    <div className="space-y-5">
      <Header />
      {/* <Component /> */}
      {children}
      <Footer />
    </div>
  );
}
