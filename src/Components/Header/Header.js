import React from "react";
import UserNav from "./UserNav";
import { NavLink } from "react-router-dom";

export default function Header({children}) {
  return (
    <div className="flex px-10 py-5 shadow shadow-black-200 justify-between items-center">
      <NavLink to="/">
        <span className="text-red-500 font-medium text-xl">CyberFlix - {children}</span>
      </NavLink>
      <UserNav />
    </div>
  );
}
