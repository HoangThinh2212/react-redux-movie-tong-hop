import { userService } from "../../services/userServices";
import { SET_USER_INFOR } from "./../constant/userConstant";
import { userLocalService } from "../../services/localStorageService";
import { message } from "antd";

export const setLoginAction = (value) => {
  return {
    type: SET_USER_INFOR,
    payload: value,
  };
};

export const setLoginActionService = (userForm, onSuccess) => {
  return (dispatch) => {
    userService
      .postDangNhap(userForm)
      .then((res) => {
        message.success("Đăng nhập được rùiiiii");

        // Lưu vào Local Storage
        userLocalService.set(res.data.content);
        console.log(res);
        // đẩy lên redux store
        dispatch({
          type: SET_USER_INFOR,
          payload: res.data.content,
        });
        onSuccess();
      })
      .catch((err) => {
        message.error("Lỗi rồiiii");
        console.log(err);
      });
  };
};
